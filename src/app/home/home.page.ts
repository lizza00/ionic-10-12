import {Component} from '@angular/core';
import {Bank, DataGetterService} from '../service/data-getter.service';
import {Events} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {DataExchangerService} from '../service/data-exchanger.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {
    banks: Bank[];

    showNew = false;
    showEdit = -1;
    private userName = this.dataGetter.getUser();
    dataFromClients: string;
    extraData: string;

    constructor(private dataGetter: DataGetterService,
                private events: Events,
                private router: Router,
                private dataExchanger: DataExchangerService) {
        this.dataGetter.getBanks().subscribe(
            (data) => this.banks = data
        );
        this.userName = dataGetter.getUser();
    }

    add() {
        this.showNew = true;
    }

    delete(index: number) {
        this.dataGetter.deleteBank(index);
    }

    addBank(bank) {
        this.dataGetter.addBank(bank);
        this.showNew = false;
    }

    getData() {
        this.dataExchanger.setData('myData', this.extraData);
        this.router.navigate(['/data-sender']);
    }

    ionViewDidEnter() {
        this.extraData = this.dataExchanger.getData('myData');

    }
}
