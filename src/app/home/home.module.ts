import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {HomePage} from './home.page';
import {BankComponent} from '../components/bank/bank.component';
import {ClientComponent} from '../client/client.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: HomePage
            }
        ])
    ],
    exports: [
        ClientComponent
    ],
    declarations: [HomePage, BankComponent, ClientComponent]
})
export class HomePageModule {
}
