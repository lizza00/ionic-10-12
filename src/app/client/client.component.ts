import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Client} from '../service/data-getter.service';

@Component({
    selector: 'app-client',
    templateUrl: './client.component.html',
    styleUrls: ['./client.component.scss'],
})
export class ClientComponent implements OnInit {
  @Input() client: Client = {
    name: '', account: '', balance: 0, bank: ''
  };

  @Input() isNew: boolean;
  @Output() addClient = new EventEmitter();
  @Output() cancelAddingClient = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  addNew() {
    if (this.isNew) {
      this.addClient.emit(this.client);
    }
  }

  cancelAdding() {
    if (this.isNew) {
      this.cancelAddingClient.emit();
    }
  }
}
