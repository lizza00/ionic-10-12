import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Bank} from '../../service/data-getter.service';

@Component({
    selector: 'app-bank',
    templateUrl: './bank.component.html',
    styleUrls: ['./bank.component.scss'],
})
export class BankComponent implements OnInit {

    @Input() bank: Bank = {
        name: '',
        money: 0,
        address: '',
        filials: 0
    };
    @Input() isNew: boolean;
    @Output() addBank = new EventEmitter();
    @Output() cancelAddingBank = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    addNew() {
        if (this.isNew) {
            this.addBank.emit(this.bank);
        }
    }

    cancelAdding() {
        if (this.isNew) {
            this.cancelAddingBank.emit();
        }
    }
}
