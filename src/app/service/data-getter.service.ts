import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';

export interface Bank {
    name: string;
    money: number;
    address: string;
    filials: number;
}

export interface Client {
    name: string;
    account: string;
    balance: number;
    bank: string;
}

@Injectable({
    providedIn: 'root'
})
export class DataGetterService {

    private banks: Bank[] = [
        {
            name: 'MonoBank',
            money: 11551,
            address: 'Street',
            filials: 2
        },
        {
            name: 'PrivatBank',
            money: 308381,
            address: 'Street',
            filials: 3
        }
    ];

    private clients: Client[] = [
        {name: 'Alex', account: '37352641534', balance: 5376546, bank: 'PrivatBank'},
        {name: 'Sonya', account: '24343432432', balance: 3243, bank: 'PrivatBank'},
        {name: 'Liza', account: '3735342322434', balance: 234, bank: 'PrivatBank'},
        {name: 'Dasha', account: '24343243242', balance: 341324, bank: 'MonoBank'},
        {name: 'Oleg', account: '231444444443124', balance: 533432476546, bank: 'MonoBank'},
        {name: 'Denis', account: '234324234244', balance: 373436546, bank: 'MonoBank'}
    ];

    private userName = '';

    private users = [
        'liza', 'admin'
    ];

    getUser() {
        return this.userName;
    }

    setUser(name: string) {
        this.userName = name;
    }

    userExists(name: string): boolean {
        return this.users.indexOf(name) !== -1;
    }

    constructor() {
    }

    getBanks(): Observable<Bank[]> {
        return of(this.banks);
    }

    addBank(bank: Bank) {
        this.banks.push(bank);
    }

    addClient(client: Client) {
        this.clients.push(client);
    }

    deleteBank(index) {
        this.banks.splice(index, 1);
    }

    deleteClient(name, index) {
        if (name === 'MonoBank') {
            index = index + this.clients.filter(e => e.bank === 'PrivatBank').length;
        }
        this.clients.splice(index, 1);
    }

    getClients(bankname: string): Observable<any[]> {
        return of(this.clients.filter(elem => {
            return elem.bank === bankname;
        }));
    }
}
