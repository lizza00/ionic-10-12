import { TestBed } from '@angular/core/testing';

import { DataExchangerService } from './data-exchanger.service';

describe('DataExchangerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataExchangerService = TestBed.get(DataExchangerService);
    expect(service).toBeTruthy();
  });
});
