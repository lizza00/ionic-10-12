import {Injectable} from '@angular/core';

interface DataElem {
    k: string;
    d: any;
}

@Injectable({
    providedIn: 'root'
})
export class DataExchangerService {

    data: DataElem[] = [];

    constructor() {
    }

    getData(key: string) {
        const arr = this.data.filter(elem => elem.k === key);
        if (arr.length) {
            return arr[0].d;
        }
        return '';
    }

    setData(key: string, data: any) {
        const arr = this.data.filter(elem => elem.k === key);
        if (arr.length) {
            arr[0].d = data;
        } else {
            this.data.push({k: key, d: data});
        }
    }
}
