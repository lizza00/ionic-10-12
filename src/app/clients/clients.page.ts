import {Component, OnInit} from '@angular/core';
import {DataGetterService} from '../service/data-getter.service';
import {ActivatedRoute} from '@angular/router';
import {Events, NavController} from '@ionic/angular';

@Component({
    selector: 'app-clients',
    templateUrl: './clients.page.html',
    styleUrls: ['./clients.page.scss'],
})
export class ClientsPage implements OnInit {

    showNew = false;
    showEdit = -1;
    bankname: string;
    clients: any[];

    constructor(private dataGetterService: DataGetterService,
                private route: ActivatedRoute,
                private events: Events,
                private navCtrl: NavController) {
    }

    ngOnInit() {
        this.bankname = this.route.snapshot.paramMap.get('bankname');
        this.dataGetterService.getClients(this.bankname).subscribe(data => this.clients = data);
    }

    passData(data: string) {
        this.events.publish('clients:data', data);
        this.navCtrl.back();
    }

    add() {
        this.showNew = true;
    }

    delete(index: number) {
        this.dataGetterService.deleteClient(this.bankname, index);
        this.dataGetterService.getClients(this.bankname).subscribe(data => this.clients = data);
    }

    addClient(client) {
        client.bank = this.bankname;
        this.dataGetterService.addClient(client);
        this.showNew = false;
        this.dataGetterService.getClients(this.bankname).subscribe(data => this.clients = data);
    }
}
