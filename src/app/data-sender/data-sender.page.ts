import {Component, OnInit} from '@angular/core';
import {Events, NavController} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';
import {DataExchangerService} from '../service/data-exchanger.service';

@Component({
    selector: 'app-data-sender',
    templateUrl: './data-sender.page.html',
    styleUrls: ['./data-sender.page.scss'],
})
export class DataSenderPage implements OnInit {

    textData: string;

    constructor(private dataExchangerService: DataExchangerService,
                private router: Router) {
        this.textData = this.dataExchangerService.getData('myData');
    }

    ngOnInit() {
    }

    passData() {
        this.dataExchangerService.setData('myData', this.textData);
        this.router.navigate(['/home']);
    }
}
